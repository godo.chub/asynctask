﻿using System.Collections.Generic;
using System;
using System.Threading.Tasks;
using System.Linq;

public class AsyncClass
{
    public static void Main()
    {
        CallMethod();
    }

    public static async void CallMethod()
    {
        Task<int[]> MethodOne = CreateArray();

        Console.WriteLine(string.Join(",", MethodOne.Result));

        Task<int[]> MethodTwo = MultiplicateArray(await MethodOne);

        Console.WriteLine(string.Join(",", MethodTwo.Result));

        Task<int[]> MethodThree = FormatArray(await MethodTwo);

        Console.WriteLine(string.Join(",", MethodThree.Result));

        Task<double> MethodFour = Average(await MethodThree);

        Console.WriteLine(string.Join(",", MethodFour.Result));
    }

    public static async Task<int[]> CreateArray()
    {
        int Min = 0;
        int Max = 50;

        int[] arr = new int[5];

        Random randNum = new();
        await Task.Run(() =>
        {
            for (int i = 0; i < arr.Length; i++)
            {
                arr[i] = randNum.Next(Min, Max);
            }
        });

        return arr;
    }

    public static async Task<int[]> MultiplicateArray(int[] arr)
    {
        int Min = 0;
        int Max = 50;
        Random random = new();
        int multiplier = random.Next(Min, Max);

        await Task.Run(() =>
        {
            for (int i = 0; i < arr.Length; ++i)
                arr[i] *= multiplier;
        });

        return arr;
    }

    public static Task<int[]> FormatArray(int[] arr)
    {
        return Task.FromResult(arr.OrderBy(x => x).ToArray());
    }

    public static Task<double> Average(int[] arr)
    {
        return Task.FromResult(Queryable.Average(arr.AsQueryable()));
    }
}